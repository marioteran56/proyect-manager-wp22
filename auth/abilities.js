const { AbilityBuilder, PureAbility } = require("@casl/ability");
const Member = require('../models/member');
const mongoose = require('mongoose');
 
async function defineRulesFor(member) {
    const { can, rules } = new AbilityBuilder(PureAbility);

    // Sin usuario no hay reglas
    if (!member) return new PureAbility(rules);
        
    let permissions = await Member.aggregate([
        {$match: {"_email": member}},
        {$project: { "_id": 0, "_profiles": 1 }},
        {$lookup:
            {
                from: "profiles",
                localField: "_profiles",
                foreignField: "_id",
                as: "_profiles"
            }
        },
        {$project: { "_id": 0, "_profiles._permissions": 1 }},
        {$lookup:
            {
                from: "permissions",
                localField: "_profiles._permissions",
                foreignField: "_id",
                as: "_permissions"
            }
        },
        {$project: { "_id": 0, "_permissions": 1 }}
    ]);

    const userPermissions = permissions[0]._permissions;

    for (let index = 0; index < userPermissions.length; index++) {
        can(userPermissions[index]._type, mongoose.model(userPermissions[index]._description));
    }
    
    return new PureAbility(rules);
}
 
module.exports = { defineRulesFor };
