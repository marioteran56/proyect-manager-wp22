const express = require("express");
const Proyect = require("../models/proyect");
const Backlog = require("../models/backlog");
const Member = require("../models/member");
const { ForbiddenError } = require("@casl/ability");

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Proyect);
      Proyect.find()
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.proyects"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.proyects"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Proyect);
      const id = req.params.id;
      Proyect.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.proyects")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.proyects")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function create(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Proyect);
      const title = req.body.title;
      const requestDate = req.body.requestDate;
      const startDate = req.body.startDate;
      const description = req.body.description;
      const managerId = req.body.managerId;
      const ownerId = req.body.ownerId;
      const teamId = req.body.teamId;
      const controlPanelId = req.body.controlPanelId;

      let manager = await Member.findOne({ _id: managerId });
      let owner = await Member.findOne({ _id: ownerId });
      let team = await Member.find({ _id: teamId });
      let controlPanel = await Backlog.find({ _id: controlPanelId });

      let proyect = new Proyect({
        title: title,
        requestDate: requestDate,
        startDate: startDate,
        description: description,
        manager: manager,
        owner: owner,
        team: team,
        controlPanel: controlPanel,
      });

      proyect
        .save()
        .then((obj) =>
          res.status(200).json({
            message: res.__("create.proyects"),
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badCreate.proyects"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function replace(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Proyect);
      const id = req.params.id;
      const title = req.body.title ? req.body.title : "";
      const requestDate = req.body.requestDate
        ? req.body.requestDate
        : Date.now();
      const startDate = req.body.startDate ? req.body.startDate : Date.now();
      const description = req.body.description ? req.body.description : "";
      const managerId = req.body.managerId ? req.body.managerId : "";
      const ownerId = req.body.ownerId ? req.body.ownerId : "";
      const teamId = req.body.teamId ? req.body.teamId : [];
      const controlPanelId = req.body.controlPanelId
        ? req.body.controlPanelId
        : [];

      let manager = await Member.findOne({ _id: managerId });
      let owner = await Member.findOne({ _id: ownerId });
      let team = await Member.find({ _id: teamId });
      let controlPanel = await Backlog.find({ _id: controlPanelId });

      let proyect = new Object({
        _title: title,
        _requestDate: requestDate,
        _startDate: startDate,
        _description: description,
        _manager: manager,
        _owner: owner,
        _team: team,
        _controlPanel: controlPanel,
      });

      Proyect.findOneAndUpdate({ _id: id }, proyect, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("replace.proyects")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badReplace.proyects")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function update(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Proyect);
      const id = req.params.id;
      const title = req.body.title;
      const requestDate = req.body.requestDate;
      const startDate = req.body.startDate;
      const description = req.body.description;
      const managerId = req.body.managerId;
      const ownerId = req.body.ownerId;
      const teamId = req.body.teamId;
      const controlPanelId = req.body.controlPanelId;

      let manager = await Member.findOne({ _id: managerId });
      let owner = await Member.findOne({ _id: ownerId });
      let team = await Member.find({ _id: teamId });
      let controlPanel = await Backlog.find({ _id: controlPanelId });

      let proyect = new Object();

      if (title) proyect._title = title;
      if (requestDate) proyect._requestDate = requestDate;
      if (startDate) proyect._startDate = startDate;
      if (description) proyect._description = description;
      if (manager) proyect._manager = manager;
      if (owner) proyect._owner = owner;
      if (team) proyect._team = team;
      if (controlPanel) proyect._controlPanel = controlPanel;

      Proyect.findOneAndUpdate({ _id: id }, proyect, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("update.proyects")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badUpdate.proyects")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Proyect);
      const id = req.params.id;
      Proyect.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.proyects")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.proyects")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = { list, index, create, replace, update, destroy };
