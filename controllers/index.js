const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("config");
const Member = require("../models/member");

function home(req, res, next) {
  res.render("index", { title: "Express" });
}

function login(req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

  Member.findOne({ _email: email })
    .select("_password _salt")
    .then((member) => {
      console.log(member);
      if (member) {
        bcrypt.hash(password, member.salt, (err, hash) => {
          if (err) {
            res.status(403).json({
              message: res.__("bad.login"),
              obj: err,
            });
          }
          if (hash === member.password) {
            const jwtKey = config.get("secret.key");
            const token = jwt.sign({ data: email }, jwtKey, {
              expiresIn: "1hr",
            });
            req.token = token;
            res.status(200).json({
              message: res.__("ok.login"),
              obj: token,
            });
          } else {
            res.status(403).json({
              message: res.__("bad.login"),
              obj: null,
            });
          }
        });
      } else {
        res.status(403).json({
          message: res.__("bad.login"),
          obj: null,
        });
      }
    })
    .catch((err) => {
      res.status(403).json({
        message: res.__("bad.login"),
        obj: null,
      });
    });
}

module.exports = { home, login };
