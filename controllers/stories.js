const express = require("express");
const Story = require("../models/story");
const { ForbiddenError } = require("@casl/ability");

const Priority = ["Low", "Medium", "High"];

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Story);
      Story.find()
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.stories"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.stories"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Story);
      const id = req.params.id;
      Story.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.stories")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.stories")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function create(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Story);
      const narrative = req.body.narrative;
      // const role = req.body.role;
      const functionality = req.body.functionality;
      const benefit = req.body.benefit;
      const priority = req.body.priority;
      const size = req.body.size;
      const acceptanceCriteria = req.body.acceptanceCriteria;
      const context = req.body.context;
      const events = req.body.events;
      const results = req.body.results;
      const valid = req.body.valid;

      let story = new Story({
        narrative: narrative,
        // role: role,
        functionality: functionality,
        benefit: benefit,
        priority: priority,
        size: size,
        acceptanceCriteria: acceptanceCriteria,
        context: context,
        events: events,
        results: results,
        valid: valid,
      });

      if (!Priority.includes(story.priority)) {
        res.status(400).json({
          message: res.__("badPriority.stories"),
          obj: null,
        });
      } else {
        story
          .save()
          .then((obj) =>
            res.status(200).json({
              message: res.__("create.stories"),
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: res.__("badCreate.stories"),
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function addEvents(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Story);
      const id = req.params.id;
      const event = req.body.event;
      Story.findByIdAndUpdate(
        { _id: id },
        { $push: { _events: event } },
        { upsert: true, new: true }
      )
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("addEvent.stories")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badAddEvents.stories")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function removeEvents(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Story);
      const id = req.params.id;
      const event = req.body.event;
      Story.findByIdAndUpdate(
        { _id: id },
        { $pull: { _events: event } },
        { upsert: true, new: true }
      )
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("removeEvents.stories")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badRemoveEvents.stories")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function addResults(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Story);
      const id = req.params.id;
      const result = req.body.result;
      Story.findByIdAndUpdate(
        { _id: id },
        { $push: { _results: result } },
        { upsert: true, new: true }
      )
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("addResults.stories")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badAddResults.stories")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function removeResults(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Story);
      const id = req.params.id;
      const result = req.body.result;
      Story.findByIdAndUpdate(
        { _id: id },
        { $pull: { _results: result } },
        { upsert: true, new: true }
      )
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("removeResults.stories")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badRemoveResults.stories")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function replace(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Story);
      const id = req.params.id;
      let narrative = req.body.narrative ? req.body.narrative : "";
      let role = req.body.role ? req.body.role : "";
      let functionality = req.body.functionality ? req.body.functionality : "";
      let benefit = req.body.benefit ? req.body.benefit : "";
      let priority = req.body.priority ? req.body.priority : "";
      let size = req.body.size ? req.body.size : -1;
      let acceptanceCriteria = req.body.acceptanceCriteria
        ? req.body.acceptanceCriteria
        : "";
      let context = req.body.context ? req.body.context : "";
      let events = req.body.events ? req.body.events : [];
      let results = req.body.results ? req.body.results : [];
      let valid = req.body.valid ? req.body.valid : false;

      let story = new Object({
        _narrative: narrative,
        _role: role,
        _functionality: functionality,
        _benefit: benefit,
        _priority: priority,
        _size: size,
        _acceptanceCriteria: acceptanceCriteria,
        _context: context,
        _events: events,
        _results: results,
        _valid: valid,
      });

      if (!Priority.includes(story._priority)) {
        res.status(400).json({
          message: res.__("badPriority.stories"),
          obj: null,
        });
      } else {
        Story.findOneAndUpdate({ _id: id }, story, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("replace.stories")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badReplace.stories")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function update(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Story);
      const id = req.params.id;
      let narrative = req.body.narrative;
      let role = req.body.role;
      let functionality = req.body.functionality;
      let benefit = req.body.benefit;
      let priority = req.body.priority;
      let size = req.body.size;
      let acceptanceCriteria = req.body.acceptanceCriteria;
      let context = req.body.context;
      let events = req.body.events;
      let results = req.body.results;
      let valid = req.body.valid;

      let story = new Object();

      if (narrative) story._narrative = narrative;
      if (role) story._role = role;
      if (functionality) story._functionality = functionality;
      if (benefit) story._benefit = benefit;
      if (priority) story._priority = priority;
      if (size) story._size = size;
      if (acceptanceCriteria) story._acceptanceCriteria = acceptanceCriteria;
      if (context) story._context = context;
      if (events) story._events = events;
      if (results) story._results = results;
      if (valid) story._valid = valid;

      if (priority != undefined && !Priority.includes(story._priority)) {
        res.status(400).json({
          message: res.__("badPriority.stories"),
          obj: null,
        });
      } else {
        Story.findOneAndUpdate({ _id: id }, story, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("update.stories")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badUpdate.stories")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Story);
      const id = req.params.id;
      Story.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.stories")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.stories")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = {
  list,
  index,
  create,
  replace,
  update,
  destroy,
  addEvents,
  removeEvents,
  addResults,
  removeResults,
};
