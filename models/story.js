const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _narrative: String,
    // _role: String,
    _functionality: String,
    _benefit: String,
    _priority: String,
    _size: Number,
    _acceptanceCriteria: String,
    _context: String,
    _events: [String],
    _results: [String],
    _valid: Boolean
});

class Story {
    constructor(narrative, functionality, benefit, priority, size, acceptanceCriteria, context, events, results, valid){
        this._narrative = narrative;
        // this._role = role;
        this._functionality = functionality;
        this._benefit = benefit;
        this._priority = priority;
        this._size = size;
        this._acceptanceCriteria = acceptanceCriteria;
        this._context = context;
        this._events = events;
        this._results = results;
        this._valid = valid;
    }

    get narrative(){
        return this._narrative;
    }

    set narrative(narrative){
        this._narrative = narrative;
    }

    // get role(){
    //     return this._role;
    // }

    // set role(role){
    //     this._role = role;
    // }

    get functionality(){
        return this._functionality;
    }

    set functionality(functionality){
        this._functionality = functionality;
    }

    get benefit(){
        return this._benefit;
    }

    set benefit(benefit){
        this._benefit = benefit;
    }

    get priority(){
        return this._priority;
    }

    set priority(priority){
        this._priority = priority;
    }

    get size(){
        return this._size;
    }

    set size(size){
        this._size = size;
    }

    get acceptanceCriteria(){
        return this._acceptanceCriteria;
    }

    set acceptanceCriteria(acceptanceCriteria){
        this._acceptanceCriteria = acceptanceCriteria;
    }

    get context(){
        return this._context;
    }

    set context(context){
        this._context = context;
    }

    get events(){
        return this._events;
    }

    set events(events){
        this._events = events;
    }

    get results(){
        return this._results;
    }

    set results(results){
        this._results = results;
    }

    get valid(){
        return this._valid;
    }

    set valid(valid){
        this._valid = valid;
    }
}

schema.loadClass(Story);
module.exports = mongoose.model('Story', schema);