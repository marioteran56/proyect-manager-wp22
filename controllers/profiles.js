const express = require("express");
const Profile = require("../models/profile");
const Permission = require("../models/permission");
const { ForbiddenError } = require("@casl/ability");

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Profile);
      Profile.find()
        .populate("_permissions")
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.profiles"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.profiles"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Profile);
      const id = req.params.id;
      Profile.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.profiles")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.profiles")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function create(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Profile);
      const description = req.body.description;
      const status = req.body.status;
      const permissionsId = req.body.permissionsId;

      let permissions = await Permission.find({ _id: permissionsId });

      let profile = new Profile({
        description: description,
        status: status,
        permissions: permissions,
      });

      profile
        .save()
        .then((obj) =>
          res.status(200).json({
            message: res.__("create.profiles"),
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badCreate.profiles"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function replace(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Profile);
      const id = req.params.id;
      const description = req.body.description ? req.body.description : "";
      const status = req.body.status ? req.body.status : false;
      const permissionsId = req.body.permissionsId
        ? req.body.permissionsId
        : [];

      let permissions = await Permission.find({ _id: permissionsId });

      let profile = new Object({
        _description: description,
        _status: status,
        _permissions: permissions,
      });

      Profile.findOneAndUpdate({ _id: id }, profile, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("replace.profiles")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badReplace.profiles")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function update(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Profile);
      const id = req.params.id;
      const description = req.body.description;
      const status = req.body.status;
      const permissionsId = req.body.permissionsId;

      let permissions = await Permission.find({ _id: permissionsId });

      let profile = new Object();

      if (description) profile._description = description;
      if (status) profile._status = status;
      if (permissions) profile._permissions = permissions;

      Profile.findOneAndUpdate({ _id: id }, profile, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("update.profiles")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badUpdate.profiles")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Profile);
      const id = req.params.id;
      Profile.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.profiles")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.profiles")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = { list, index, create, replace, update, destroy };
