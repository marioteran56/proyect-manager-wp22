const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");
const config = require("config");
const { expressjwt: jwt } = require("express-jwt");
const auth = require('./auth')
const i18n = require("i18n");

var indexRouter = require("./routes/index");
var abilitiesRouter = require("./routes/abilities");
var backlogsRouter = require("./routes/backlogs");
var membersRouter = require("./routes/members");
var proyectsRouter = require("./routes/proyects");
var storiesRouter = require("./routes/stories");
var permissionsRouter = require("./routes/permissions");
var profilesRouter = require("./routes/profiles");

// CONECTION DATABSE "mongodb://<dbUser>?:<dbPassword>?@<direction>:<port>/<dbName>"
let uri = config.get("dbChain");

mongoose.connect(uri);
const db = mongoose.connection;

const app = express();

db.on("open", () => {
  console.log("CONECCTION SUCCESS");
});

db.on("error", () => {
  console.log("CONECCTION ERROR");
});

i18n.configure({
  locales: ["en", "es"],
  cookie: "language",
  directory: path.join(__dirname, "locales"),
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(i18n.init);

const jwtKey = config.get("secret.key");
app.use(
  jwt({ secret: jwtKey, algorithms: ["HS256"] }).unless({ path: ["/login"] })
);

app.use("/", indexRouter);
auth.configure(app);
app.use("/abilities", abilitiesRouter);
app.use("/backlogs", backlogsRouter);
app.use("/members", membersRouter);
app.use("/proyects", proyectsRouter);
app.use("/stories", storiesRouter);
app.use("/permissions", permissionsRouter);
app.use("/profiles", profilesRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
