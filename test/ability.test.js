const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear abilities", () => {
  it("Deberia crear una ability", (done) => {
    supertest(app)
      .post("/abilities")
      .send({
        description: "Desarrollo frontend svelt",
        rank: "JUNIOR",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia crear una ability", (done) => {
    supertest(app)
      .post("/abilities")
      .send({
        description: "Desarrollo frontend svelt",
        rank: "JUNIORRR",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener todas las abilities", () => {
  it("Deberia obtener la lista de ability", (done) => {
    supertest(app)
      .get("/abilities")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener una ability con id", () => {
  it("Deberia obtener una ability", (done) => {
    supertest(app)
      .get("/abilities/638719bbf789c36127db32cf")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("Deberia obtener una ability", (done) => {
    supertest(app)
      .get("/abilities/638719bbf789c36127db32zk")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar remplazar una ability con id", () => {
  it("Deberia remplazar una ability", (done) => {
    supertest(app)
      .patch("/abilities/638719bbf789c36127db32cf")
      .send({
        description: "Desarrollo frontend svelt",
        rank: "JUNIOR",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia remplazar una ability", (done) => {
    supertest(app)
      .patch("/abilities/638719bbf789c36127db32zk")
      .send({
        description: "Desarrollo frontend svelt",
        rank: "JUNIORRRR",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar una ability con id", () => {
  it("Deberia actualizar una ability", (done) => {
    supertest(app)
      .put("/abilities/638719bbf789c36127db32cf")
      .send({
        description: "Desarrollo frontend svelt",
        rank: "SENIOR",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia actualizar una ability", (done) => {
    supertest(app)
      .put("/abilities/638719bbf789c36127db32zk")
      .send({
        description: "Desarrollo frontend svelt",
        rank: "SENIORR",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar una ability con id", () => {
  it("Deberia actualizar una ability", (done) => {
    supertest(app)
      .delete("/abilities/638719bbf789c36127db32cf")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia eliminar una ability", (done) => {
    supertest(app)
      .delete("/abilities/638719bbf789c36127db32zk")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
