const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _title: String,
    _requestDate: Date,
    _startDate: Date,
    _description: String,
    _manager: {
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    },
    _owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    },
    _team: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    }],
    _controlPanel: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Backlog'
    }]
});

class Proyect {
    constructor(title, requestDate, startDate, description, manager, owner, team, controlPanel){
        this._title = title;
        this._requestDate = requestDate;
        this._startDate = startDate;
        this._description = description;
        this._manager = manager;
        this._owner = owner;
        this._team = team;
        this._controlPanel = controlPanel;
    }

    get title(){
        return this._title;
    }

    set title(title){
        this._title = title;
    }

    get requestDate(){
        return this._requestDate;
    }

    set requestDate(requestDate){
        this._requestDate = requestDate;
    }

    get startDate(){
        return this._startDate;
    }

    set startDate(startDate){
        this._startDate = startDate;
    }

    get description(){
        return this._description;
    }

    set description(description){
        this._description = description;
    }

    get manager(){
        return this._manager;
    }

    set manager(manager){
        this._manager = manager;
    }

    get owner(){
        return this._owner;
    }

    set owner(owner){
        this._owner = owner;
    }

    get team(){
        return this._team;
    }

    set team(team){
        this._team = team;
    }

    get controlPanel(){
        return this._controlPanel;
    }

    set controlPanel(controlPanel){
        this._controlPanel = controlPanel;
    }

}

schema.loadClass(Proyect);
module.exports = mongoose.model('Proyect', schema);