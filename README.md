
# Proyecto reto I: Desarrollar un manejador de proyectos 

Primer parcial – Se desarrollo un proyecto con hecho en node con la ayuda del framework de express y la ayuda de Docker, en el cual solo se crearon las puras rutas y controladores permitiendo tener una buena distribución del proyecto para futuras incorporaciones de características nuevas adicionales al proyecto.

## Diagrama de clases

![](https://gitlab.com/marioteran56/proyect-manager-wp22/-/raw/main/diagramClass.jpg)

- CRUD **Enum**
	Contiene las acciones permitidas en un enum, las cuales son CREATE, READ, UPDATE y DELETE.
- Rank **Enum**
	Las habilidades ranqueadas son JUNIOR, SENIOR y MASTER
- Priority **Enum**
	Las prioridades ortogradas de las cuales son LOW, MEDIUM y HIGH
- BacklogType **Enum**
	Son el tipo de columna, las cuales son PRODUCT_BACKLOG, RELEASE_BACKLOG y SPRINT_BACKLOG
- Permission **Class**
	+ description: **String** -> Es el nombre al del controlador al que hace referencia
	+ type: **Enum<CRUD>** -> Tipo de accion de que puede realizar
- Profile **Class**
	+ description: **String** -> Es el nombre al que hace referencia
	+ status: **Boolean** -> Se encuentra activo o no
	+ permissions: **List<Ref<Permissions'>>** -> Lista de permisos
- Abilities **Class**
	+ description: **String** -> Es el nombre al que hace referencia 
	+ rank: **Enum<Rank´>** -> Habilidad ranqueada
- Address **Class**
	+ street: **String** -> Nombre de la calle
	+ intNumber: **Number** -> Numero interior de la casa o departamento
	+ extNumber: **Number** -> Numero interior de la casa o departamento
	+ state: **String** -> Nombre del estado
	+ country: **String** -> Nombre del pais
- Story **Class**
	+ narrative: **String** -> nombre de la narrativa
	+ role: **String** -> nombre del rol
	+ functionality: **String** -> Funcionalidad
	+ priority: **Enum<Priority'>** -> Prioridad
	+ size: **Number** -> Tamaño 
	+ acceptanceCriteria: **String** -> Criterio de aceptacion
	+ context: **String** -> Contexto
	+ events: **List<String´>** -> Lista de eventos
	+ results: **List<String´>** -> Lista de resultados
	+ valid: **Boolean** -> Valido
- Backlog **Class**
	+ type: **Enum<BacklogType'>** -> Tipo de columna a la que pertenece
	+ stories: **List<Ref<Story´>>** -> Lista de historias a las que hace referencia
- Proyect **Class**
	+ title: **String** -> Nombre del proyecto
	+ requestDate: **Date** -> Fecha de solicitud
	+ startDate: **Date** -> Fecha de inicio
	+ description: **String** -> Descripcion del proyecto
	+ manager: **Ref<Member´>** -> Referencia del manager del proyecto
	+ owner: **Ref<Member'>** -> Referencia del dueño del proyecto
	+ team: **List<Ref<Member'>>** -> Lista del equipo que estaran en el proyecto
	+ controlPanel: **List<Backlog'>** -> Lista de columnas asignadas al proyecto
- Member **Class**
	+ email: **String** -> Correo electronico
	+ password: **String** -> Contraseña
	+ salt: **String** -> Número de dígitos aleatorios que se le agrega a la contraseña
	+ fullName: **String** -> Nombre completo de la persona
	+ birthdate: **Date** -> Fecha de nacimiento
	+ curp: **String** -> CURP de la persona
	+ rfc: **String** -> Registro Federal de Contribuyentes
	+ address: **Embeded<Address'>** -> Direccion en la que vive
	+ abilities: **List<Ref<Ability'>>** -> Lista de habilidades de la persona
	+ profiles: **List<Ref<Profile>>** -> Lista de perfiles

## Diagrama de secuencia

![](https://gitlab.com/marioteran56/proyect-manager-wp22/-/raw/main/diagramSeq.png)

1. Open proyect manager
	Abre el proyecto el proyecto
	1.  Login(socialMedia)
	Hace login con las credenciales
	2. Token
	Reegresa un token una vez hecho login
	3. Grant member access
	El token es valido
	4. Denny access
	El token no es valido
2. Set story size
	Establece el tamaño de la historia, con la secuencia de fibonacci
	1. setSize(fibo)
	Actualizar el valor de la secuencia de fibonacci
	2. getSize
	Obtener el valor de la sencuencia de fibonacci
3. Update backlog stories
	Actualizar historia de trabajo pendiente
	1. Get story
	Obtener alguna historia especifica
	2. Story
	Regresa la historia especifica
	3. updateStory(story)
	Actualiza una historia especifica
4. Get development velocity
	Obtener velocidad de desarrollo del backlog
	1. developmentSpeed()
	Regresa la velocidad de desarrollo del backlog
5. Create burndown chart
	Crear gráfico de trabajo pendiente del backlog
	1. burndownChart
	Regresa la grafica de los trabajos pendientes del backlog
6. Get proyect time estimate
	Obtener el tiempo estimado de un proyecto
	1. timeEstimated()
	Regresa el tiempo estiamdo del proyecto
7. Monitor proyect
	El scrum master debe poder monitorear el proyecto y monitoriar el proyecto
	1. Check user role
	Checa que el rol sea un scrum master
	2. Get proyect
	Obtenemos el proyecto scrum master
	3. Proyect
	Nos regresa el proyecto en caso de ser un scrum master
	4. monitorProyect(proyect)
	regresa el monitor del proyecto, el cual para eso debio ser un scrum master
	5. return
	No regresa nada, debido que no es un scrum master
8. Check stories completion
	Verifica la finalización de las historias
	1. Check user role
	Verifica que sea un scrum master
	2. Get story
	Obtenermos la historia si se valido correctammete el rol como scrum master
	3. Story
	Nos regresa la historia si se valido correctammete el rol como scrum master
	4. checkStoryStatus(story)
	Nos regresa si la historia ha sido o no completada, siempre cuando el rol sea de scum master.
	5. return
	No regresa nada, debido que no es un scrum master
9. Validate o discard a story
	Valida o rechaza una hostoria
	1. Check user role
	Verifica que sea un product owner
	2. Get story
	Obtenermos la historia si se valido correctammete el rol como product owner
	3. Story
	Nos regresa la historia si se valido correctammete el rol como product owner
	4. validateStory(story)
	Valida si la horia se va a rechazar o aceptar la historia, siempre y cuando este valido correctamente el rol como product owner
	5. return 
	No regresa nada, debido que no es un product owner
10. Get release retrospective
	Debe permitir realizar retrospectivas por cada release
	1. getRetrospective()
	Regresa la retrospectiva
11. Generate proyect closure
	Debe generar un cierre de proyecto
	1. Check user role
	Verifica que sea un product owner
	2. Get proyect
	Obtenemos el proyecto, siempre y cuando el rol sea un product owner
	3. proyectClosure()
	Genera un cierro del proyecto, siempre y cuando el rol sea un product owner
	4. return
	No regresa nada, debido que no es un product owner

	

## Tech Stack

**Server:** Node, Express
**Devops:** Git, Docker

## Prerequisites
Para poder correr el proyecto correctamente, se deberá tener instalado dentro de la maquina en la que se quiera correr el proyecto [Docker](https://www.docker.com/products/docker-desktop/), al igual que una versión de [Node.js](https://nodejs.org/en/).

## Installation of project

Para la instalacion del proyecto, tendra que clonarlo con el siguiente comando

```bash
  git clone git@gitlab.com:marioteran56/proyect-manager-wp22.git
```

Instalacion via npm, para poder cargar todos los package de nuestro proyecto dentro de un archivo llamado **node_modules**

```bash
  npm install
```
    
## Running

Correr en produccion
```bash
  npm start
```
Correr en desarrollo
```bash
  npm run dev
```

## Usage

Para probar que este funcionando de manera correcta, dentro de una herramienta para realizar peticiones por medio del protocolo HTTP, como `Postman`, al ejecutar el comando `POST` dentro de la liga `http://localhost:3000/abilities` y mandando como parametros del cuerpo los argumentos `description: JavaScript` y `rank: MASTER`, similar a la siguiente imagen:

![](https://gitlab.com/marioteran56/proyect-manager-wp22/-/raw/main/postmanExample.png)

Obtenemos la siguiente respuesta:

```bash
  respond with CREATE  => Javascript, MASTER
```

## Routes
Cada una de las rutas pueden ser probadas por Postman o Insomnia, en el cual cada una de las siguientes rutas seran de manera local.
- INDEX (Indice) -> **GET**
localhost:3000/
- Users (Usuarios) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/users
- Abilities (Habilidades) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/abilities
- Addresses (Direcciones) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/addresses
- Backlogs (Trabajos atrasados) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/backlogs
- Members (Miembros) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/members
- Proyects (Proyectos) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/proyects
- Roles (Roles) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/roles
- Stories (Historias) -> **GET**, **POST**, **PUT**, **DELETE**
localhost:3000/stories

## Docker container
- https://hub.docker.com/r/marioteran56/wp-proyect-manager

## Authors

- Mario Alberto Teran Acosta [@marioteran56](https://gitlab.com/marioteran56) 
- Raul Hiram Pineda Chavez [@pineda24](https://gitlab.com/pineda24)
