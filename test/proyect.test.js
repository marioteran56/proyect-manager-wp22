const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear un proyecto", () => {
  it("Deberia crear un proyecto", (done) => {
    supertest(app)
      .post("/proyects")
      .send({
        title: "FIT+",
        requestDate: "12-09-2022",
        startDate: "12-10-2022",
        description: "Aplicacion de gym",
        managerId: "63862d57dd5f34703929395f",
        ownerId: "6386f35014fc703760132749",
        teamId: "63862d57dd5f34703929395f",
        teamId: "6386f35014fc703760132749",
        controlPanelId: "63870d359e9cf2ae7212defe",
        controlPanelId: "63870c9c9e9cf2ae7212def9",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar cargar toda la lista de proyectos", () => {
  it("Deberia regresar la lista de proyectos", (done) => {
    supertest(app)
      .get("/proyects")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar cargar un proyecto especifico", () => {
  it("Deberia regresar un proyecto especifico", (done) => {
    supertest(app)
      .get("/proyects/638710f09e9cf2ae7212df40")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia regresar un proyecto especifico", (done) => {
    supertest(app)
      .get("/proyects/638710f09e9cf2ae7212df32")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar remplazar un proyecto especifico", () => {
  it("Deberia remplazar un proyecto especifico", (done) => {
    supertest(app)
      .patch("/proyects/638710f09e9cf2ae7212df40")
      .send({
        title: "FIT++",
        requestDate: "12-09-2022",
        startDate: "12-10-2022",
        description: "Aplicacion de gym para principiantes",
        managerId: "6386f35014fc703760132749",
        ownerId: "63862d57dd5f34703929395f",
        teamId: "63862d57dd5f34703929395f",
        teamId: "6386f35014fc703760132749",
        controlPanelId: "63870d359e9cf2ae7212defe",
        controlPanelId: "63870c9c9e9cf2ae7212def9",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia remplazar un proyecto especifico", (done) => {
    supertest(app)
      .patch("/proyects/638710f09e9cf2ae7212df32")
      .send({
        title: "FIT++",
        requestDate: "12-09-2022",
        startDate: "12-10-2022",
        description: "Aplicacion de gym para principiantes",
        managerId: "6386f35014fc703760132749",
        ownerId: "63862d57dd5f34703929395f",
        teamId: "63862d57dd5f34703929395f",
        teamId: "6386f35014fc703760132749",
        controlPanelId: "63870d359e9cf2ae7212defe",
        controlPanelId: "63870c9c9e9cf2ae7212def9",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar un proyecto especifico", () => {
  it("Deberia actualizar un proyecto especifico", (done) => {
    supertest(app)
      .put("/proyects/638710f09e9cf2ae7212df40")
      .send({
        title: "FIT++",
        requestDate: "12-09-2022",
        startDate: "12-10-2022",
        description: "Aplicacion de gym para principiantes",
        managerId: "6386f35014fc703760132749",
        ownerId: "63862d57dd5f34703929395f",
        teamId: "63862d57dd5f34703929395f",
        teamId: "6386f35014fc703760132749",
        controlPanelId: "63870d359e9cf2ae7212defe",
        controlPanelId: "63870c9c9e9cf2ae7212def9",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia actualizar un proyecto especifico", (done) => {
    supertest(app)
      .put("/proyects/638710f09e9cf2ae7212df32")
      .send({
        title: "FIT++",
        requestDate: "12-09-2022",
        startDate: "12-10-2022",
        description: "Aplicacion de gym para principiantes",
        managerId: "6386f35014fc703760132749",
        ownerId: "63862d57dd5f34703929395f",
        teamId: "63862d57dd5f34703929395f",
        teamId: "6386f35014fc703760132749",
        controlPanelId: "63870d359e9cf2ae7212defe",
        controlPanelId: "63870c9c9e9cf2ae7212def9",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar un proyecto especifico", () => {
  it("Deberia eliminar un proyecto especifico", (done) => {
    supertest(app)
      .delete("/proyects/638710f09e9cf2ae7212df40")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia eliminar un proyecto especifico", (done) => {
    supertest(app)
      .delete("/proyects/638710f09e9cf2ae7212df32")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
