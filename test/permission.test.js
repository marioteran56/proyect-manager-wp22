const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear un permiso", () => {
  it("Deberia crear un permiso", (done) => {
    supertest(app)
      .post("/permissions")
      .send({
        description: "Ability",
        type: "READ",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia crear un permiso", (done) => {
    supertest(app)
      .post("/permissions")
      .send({
        description: "Member",
        type: "READDD",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener todos los permisos", () => {
  it("Deberia crear un permiso", (done) => {
    supertest(app)
      .get("/permissions")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener un permiso especifico", () => {
  it("Deberia crear un permiso", (done) => {
    supertest(app)
      .get("/permissions/63872711f75ac178f4dd0962")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia crear un permiso", (done) => {
    supertest(app)
      .get("/permissions/63872711f75ac178f4dd09gs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar remplazar un permiso", () => {
  it("Deberia remplazar un permiso especifico", (done) => {
    supertest(app)
      .patch("/permissions/63872711f75ac178f4dd0962")
      .send({
        description: "Backlog",
        type: "DELETE",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia remplazar un permiso especifico", (done) => {
    supertest(app)
      .patch("/permissions/63872711f75ac178f4dd0962")
      .send({
        description: "Backlog",
        type: "DELETEEEEEE",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar un permiso", () => {
  it("Deberia remplazar un permiso especifico", (done) => {
    supertest(app)
      .put("/permissions/63872711f75ac178f4dd0962")
      .send({
        description: "Proyect",
        type: "CREATE",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia actualizar un permiso especifico", (done) => {
    supertest(app)
      .put("/permissions/63872711f75ac178f4dd0962")
      .send({
        description: "Proyect",
        type: "CREATEEEEEEEE",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar un permiso especifico", () => {
  it("Deberia eliminar un permiso especifico", (done) => {
    supertest(app)
      .delete("/permissions/63872711f75ac178f4dd0962")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia eliminar un permiso especifico", (done) => {
    supertest(app)
      .delete("/permissions/63872711f75ac178f4dd09fs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
