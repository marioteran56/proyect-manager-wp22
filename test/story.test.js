const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear una historia", () => {
  it("Deberia crear una historia", (done) => {
    supertest(app)
      .post("/stories")
      .send({
        narrative:
          "Arreglar sistema de pagos, el cual no acepta tarjeta de tipo VISA",
        functionality: "Validador para tipo de tarjeta",
        benefit: "Pagos en tipo de targeta visa",
        priority: "Low",
        size: 10,
        acceptanceCriteria: "",
        context: "PAGOS",
        events: "Exprecion de tipo de tarjeta",
        events: "Afiliacion con Visa",
        results: "Combrobaciones con expresiones de tipo de Tarjeta",
        results: "Certificado con Visa",
        valid: true,
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia crear una historia", (done) => {
    supertest(app)
      .post("/stories")
      .send({
        narrative:
          "Arreglar sistema de pagos, el cual no acepta tarjeta de tipo VISA",
        functionality: "Validador para tipo de tarjeta",
        benefit: "Pagos en tipo de targeta visa",
        priority: "Lowwwwww",
        size: 10,
        acceptanceCriteria: "",
        context: "PAGOS",
        events: "Exprecion de tipo de tarjeta",
        events: "Afiliacion con Visa",
        results: "Combrobaciones con expresiones de tipo de Tarjeta",
        results: "Certificado con Visa",
        valid: true,
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener lista de historias", () => {
  it("Deberia regresar lista de historias", (done) => {
    supertest(app)
      .get("/stories")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener una historia especifica", () => {
  it("Deberia regresar una historia especifica", (done) => {
    supertest(app)
      .get("/stories/638733a00e871a5e04859ae9")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia regresar una historia especifica", (done) => {
    supertest(app)
      .get("/stories/638733a00e871a5e04859hd0")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar remplazar una historia especifica", () => {
  it("Deberia remplazar una historia especifica", (done) => {
    supertest(app)
      .patch("/stories/638733a00e871a5e04859ae9")
      .send({
        narrative:
          "Arreglar sistema de pagos, el cual no acepta tarjeta de tipo VISA",
        functionality: "Validador para tipo de tarjeta",
        benefit: "Pagos en tipo de targeta visa",
        priority: "Low",
        size: 10,
        acceptanceCriteria: "",
        context: "PAGOS",
        events: "Exprecion de tipo de tarjeta",
        events: "Afiliacion con Visa",
        results: "Combrobaciones con expresiones de tipo de Tarjeta",
        results: "Certificado con Visa",
        valid: false,
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia remplazar una historia especifica", (done) => {
    supertest(app)
      .patch("/stories/638733a00e871a5e04859hd0")
      .send({
        narrative:
          "Arreglar sistema de pagos, el cual no acepta tarjeta de tipo VISA",
        functionality: "Validador para tipo de tarjeta",
        benefit: "Pagos en tipo de targeta visa",
        priority: "Lowwwwww",
        size: 10,
        acceptanceCriteria: "",
        context: "PAGOS",
        events: "Exprecion de tipo de tarjeta",
        events: "Afiliacion con Visa",
        results: "Combrobaciones con expresiones de tipo de Tarjeta",
        results: "Certificado con Visa",
        valid: false,
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar una historia especifica", () => {
  it("Deberia actualizar una historia especifica", (done) => {
    supertest(app)
      .put("/stories/638733a00e871a5e04859ae9")
      .send({
        narrative:
          "Arreglar sistema de pagos, el cual no acepta tarjeta de tipo VISA",
        functionality: "Validador para tipo de tarjeta",
        benefit: "Pagos en tipo de targeta visa",
        priority: "Low",
        size: 10,
        acceptanceCriteria: "",
        context: "PAGOS",
        events: "Exprecion de tipo de tarjeta",
        events: "Afiliacion con Visa",
        results: "Combrobaciones con expresiones de tipo de Tarjeta",
        results: "Certificado con Visa",
        valid: false,
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia remplazar una historia especifica", (done) => {
    supertest(app)
      .put("/stories/638733a00e871a5e04859hd0")
      .send({
        narrative:
          "Arreglar sistema de pagos, el cual no acepta tarjeta de tipo VISA",
        functionality: "Validador para tipo de tarjeta",
        benefit: "Pagos en tipo de targeta visa",
        priority: "Lowwwwww",
        size: 10,
        acceptanceCriteria: "",
        context: "PAGOS",
        events: "Exprecion de tipo de tarjeta",
        events: "Afiliacion con Visa",
        results: "Combrobaciones con expresiones de tipo de Tarjeta",
        results: "Certificado con Visa",
        valid: false,
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar una historia especifica", () => {
  it("Deberia eliminar una historia especifica", (done) => {
    supertest(app)
      .delete("/stories/638733a00e871a5e04859ae9")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia eliminar una historia especifica", (done) => {
    supertest(app)
      .delete("/stories/638733a00e871a5e04859hd0")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
