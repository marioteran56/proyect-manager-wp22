const abilities = require("./abilities");
 
module.exports = {
  configure(app) {
    app.use((req, res, next) => {
      req.ability = abilities.defineRulesFor(req.auth.data);
      next();
    });
  },
};
