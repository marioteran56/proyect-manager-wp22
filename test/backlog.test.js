const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear Backlogs", () => {
  it("Deberia crear un Backlog", (done) => {
    supertest(app)
      .post("/backlogs")
      .send({
        type: "Product Backlog",
        storiesId: "63870d359e9cf2ae7212defe",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia crear un Backlog", (done) => {
    supertest(app)
      .post("/backlogs")
      .send({
        type: "Product BacklogGG",
        storiesId: "63870d359e9cf2ae7212defe",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar busqueda de backlogs", () => {
  it("Deberia regresar la lista de Backlogs", (done) => {
    supertest(app)
      .get("/backlogs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar busqueda de un backlog especifico", () => {
  it("Deberia regresar un Backlog especifico", (done) => {
    supertest(app)
      .get("/backlogs/63870e4e9e9cf2ae7212df1a")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia regresar un Backlog especifico", (done) => {
    supertest(app)
      .get("/backlogs/63870e4e9e9cf2ae7212df6v")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar remplazar un backlog especifico", () => {
  it("Deberia reemplazar un Backlog especifico", (done) => {
    supertest(app)
      .patch("/backlogs/63870e4e9e9cf2ae7212df1a")
      .send({
        type: "Release Backlog",
        storiesId: "63870d359e9cf2ae7212defe",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia reemplazar un Backlog especifico", (done) => {
    supertest(app)
      .patch("/backlogs/63870e4e9e9cf2ae7212df1a")
      .send({
        type: "Release Backlogggggg",
        storiesId: "63870d359e9cf2ae7212defe",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar un backlog especifico", () => {
  it("Deberia actualizar un Backlog especifico", (done) => {
    supertest(app)
      .put("/backlogs/63870e4e9e9cf2ae7212df1a")
      .send({
        type: "Release Backlog",
        storiesId: "63870d359e9cf2ae7212defe",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia actualizar un Backlog especifico", (done) => {
    supertest(app)
      .put("/backlogs/63870e4e9e9cf2ae7212df1a")
      .send({
        type: "Release Backloggggg",
        storiesId: "63870d359e9cf2ae7212defe",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar un backlog especifico", () => {
  it("Deberia eliminar un Backlog especifico", (done) => {
    supertest(app)
      .delete("/backlogs/63870e4e9e9cf2ae7212df1a")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia eliminar un Backlog especifico", (done) => {
    supertest(app)
      .delete("/backlogs/63870e4e9e9cf2ae7212df1a")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
