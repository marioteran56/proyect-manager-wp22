const mongoose = require("mongoose");

const schema = mongoose.Schema({
  _description: String,
  _rank: {
    type: String,
    enum: ["JUNIOR", "SENIOR", "MASTER"],
  },
});

class Ability {
  constructor(description, rank) {
    this._description = description;
    this._rank = rank;
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  get rank() {
    return this._rank;
  }

  set rank(rank) {
    this._rank = rank;
  }
}

schema.loadClass(Ability);
module.exports = mongoose.model("Ability", schema);
