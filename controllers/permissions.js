const express = require("express");
const Permission = require("../models/permission");
const { ForbiddenError } = require("@casl/ability");

const Type = ["READ", "CREATE", "UPDATE", "DELETE", "MANAGE"];

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Permission);
      Permission.find()
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.permissions"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.permissions"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("read", Permission);
      const id = req.params.id;
      Permission.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.permissions")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.permissions")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function create(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Permission);
      const description = req.body.description;
      const type = req.body.type;

      let permission = new Permission({
        description: description,
        type: type,
      });

      if (!Type.includes(permission.type)) {
        res.status(400).json({
          message: res.__("badTypePermissions.permissions"),
          obj: null,
        });
      } else {
        permission
          .save()
          .then((obj) =>
            res.status(200).json({
              message: res.__("create.permissions"),
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: res.__("badCreate.permissions"),
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission")
        });
      }
    }
  });
}

function replace(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Permission);
      const id = req.params.id;
      let description = req.body.description ? req.body.description : "";
      let type = req.body.type ? req.body.type : "";

      let permission = new Object({
        _description: description,
        _type: type,
      });

      if (!Type.includes(permission._type)) {
        res.status(400).json({
          message: res.__("badTypePermissions.permissions"),
          obj: null,
        });
      } else {
        Permission.findOneAndUpdate({ _id: id }, permission, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("replace.permissions")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badReplace.permissions")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function update(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Permission);
      const id = req.params.id;
      let description = req.body.description;
      let type = req.body.type;

      let permission = new Object();

      if (description) permission._description = description;
      if (type) permission._type = type;

      if (type && !Type.includes(permission._type)) {
        res.status(400).json({
          message: res.__("badTypePermissions.permissions"),
          obj: null,
        });
      } else {
        Permission.findOneAndUpdate({ _id: id }, permission, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("update.permissions")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badUpdate.permissions")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Permission);
      const id = req.params.id;
      Permission.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.permissions")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.permissions")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = { list, index, create, replace, update, destroy };
