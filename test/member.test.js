const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear Miembro", () => {
  it("Deberia crear un miembro", (done) => {
    supertest(app)
      .post("/members")
      .send({
        email: "lebronJames23@gmail.com",
        password: "1234",
        fullName: "Lebron James",
        birthdate: "12-12-2022",
        curp: "MEMC770826MSLNRR62",
        rfc: "RACR221109LW3",
        abilitiesId: "63862d234f2358ce01f045eb",
        street: "Real del castor",
        intNumber: "81",
        extNumber: "30",
        zipCode: 31136,
        state: "Chihuahua",
        country: "Mexico",
        profilesId: "6386e7531c59802a8da0d048",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener lista de miembros", () => {
  it("Deberia regresar lista de miembros", (done) => {
    supertest(app)
      .get("/members")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar obtener un miembro especifico", () => {
  it("Deberia regresar un miembro especifico", (done) => {
    supertest(app)
      .get("/members/638715f38546c171417e7f20")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia regresar un miembro especifico", (done) => {
    supertest(app)
      .get("/members/638715f38546c171417e7fgs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar reemplazar Miembro", () => {
  it("Deberia reemplazar un miembro", (done) => {
    supertest(app)
      .patch("/members/638715f38546c171417e7f20")
      .send({
        email: "lebronJames23@gmail.com",
        password: "1234",
        fullName: "Lebron James",
        birthdate: "12-12-2022",
        curp: "MEMC770826MSLNRR62",
        rfc: "RACR221109LW3",
        abilitiesId: "63862d234f2358ce01f045eb",
        street: "Real del castor",
        intNumber: "81",
        extNumber: "30",
        zipCode: 31136,
        state: "Chihuahua",
        country: "Mexico",
        profilesId: "6386e7531c59802a8da0d048",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia reemplazar un miembro", (done) => {
    supertest(app)
      .patch("/members/638715f38546c171417e7fgs")
      .send({
        email: "lebronJames23@gmail.com",
        password: "1234",
        fullName: "Lebron James",
        birthdate: "12-12-2022",
        curp: "MEMC770826MSLNRR62",
        rfc: "RACR221109LW3",
        abilitiesId: "63862d234f2358ce01f045eb",
        street: "Real del castor",
        intNumber: "81",
        extNumber: "30",
        zipCode: 31136,
        state: "Chihuahua",
        country: "Mexico",
        profilesId: "6386e7531c59802a8da0d048",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar Miembro", () => {
  it("Deberia actualizar un miembro", (done) => {
    supertest(app)
      .put("/members/638715f38546c171417e7f20")
      .send({
        email: "lebronJames23@gmail.com",
        password: "1234",
        fullName: "Lebron Jamesssssss",
        birthdate: "12-12-2022",
        curp: "MEMC770826MSLNRR62",
        rfc: "RACR221109LW3",
        abilitiesId: "63862d234f2358ce01f045eb",
        street: "Real del castor",
        intNumber: "81",
        extNumber: "30",
        zipCode: 31136,
        state: "Chihuahua",
        country: "Mexico",
        profilesId: "6386e7531c59802a8da0d048",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia actualizar un miembro", (done) => {
    supertest(app)
      .put("/members/638715f38546c171417e7fgs")
      .send({
        email: "lebronJames23@gmail.com",
        password: "1234",
        fullName: "Lebron Jamessssss",
        birthdate: "12-12-2022",
        curp: "MEMC770826MSLNRR62",
        rfc: "RACR221109LW3",
        abilitiesId: "63862d234f2358ce01f045eb",
        street: "Real del castor",
        intNumber: "81",
        extNumber: "30",
        zipCode: 31136,
        state: "Chihuahua",
        country: "Mexico",
        profilesId: "6386e7531c59802a8da0d048",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar un miembro", () => {
  it("Deberia eliminar un miembro", (done) => {
    supertest(app)
      .delete("/members/638715f38546c171417e7f20")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No deberia eliminar un miembro", (done) => {
    supertest(app)
      .delete("/members/638715f38546c171417e7fgs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
