const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar crear un perfil", () => {
  it("Deberia crear un perfil", (done) => {
    supertest(app)
      .post("/profiles")
      .send({
        description: "frontdesk",
        status: true,
        permissionsId: "638632263205375dcc757b90",
        permissionsId: "638632393205375dcc757b93",
        permissionsId: "638632453205375dcc757b96",
        permissionsId: "638632513205375dcc757b99",
        permissionsId: "6386328d3205375dcc757b9b",
        permissionsId: "638632913205375dcc757b9d",
        permissionsId: "638632943205375dcc757b9f",
        permissionsId: "638632993205375dcc757ba1",
        permissionsId: "638632c83205375dcc757ba3",
        permissionsId: "638632cb3205375dcc757ba5",
        permissionsId: "638632cf3205375dcc757ba7",
        permissionsId: "638632d23205375dcc757ba9",
        permissionsId: "638632e43205375dcc757bab",
        permissionsId: "638632e73205375dcc757bad",
        permissionsId: "638632eb3205375dcc757baf",
        permissionsId: "638632ef3205375dcc757bb1",
        permissionsId: "6386df5c0580e812c35c2490",
        permissionsId: "6386df600580e812c35c2492",
        permissionsId: "6386df670580e812c35c2494",
        permissionsId: "6386df6f0580e812c35c2496",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar cargar toda la lista de perfiles", () => {
  it("Deberia cargar toda la lista de perfiles", (done) => {
    supertest(app)
      .get("/profiles")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar cargar un perfil especifico", () => {
  it("Debera cargar un perfil especifico", (done) => {
    supertest(app)
      .get("/profiles/63872dccd223515afa4b488e")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No debera cargar un perfil especifico", (done) => {
    supertest(app)
      .get("/profiles/63872dccd223515afa4b48fs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar remplazar un perfil especifico", () => {
  it("Debera remplazar un perfil especifico", (done) => {
    supertest(app)
      .patch("/profiles/63872dccd223515afa4b488e")
      .send({
        description: "frontdeskk",
        status: true,
        permissionsId: "638632263205375dcc757b90",
        permissionsId: "638632393205375dcc757b93",
        permissionsId: "638632453205375dcc757b96",
        permissionsId: "638632513205375dcc757b99",
        permissionsId: "6386328d3205375dcc757b9b",
        permissionsId: "638632913205375dcc757b9d",
        permissionsId: "638632943205375dcc757b9f",
        permissionsId: "638632993205375dcc757ba1",
        permissionsId: "638632c83205375dcc757ba3",
        permissionsId: "638632cb3205375dcc757ba5",
        permissionsId: "638632cf3205375dcc757ba7",
        permissionsId: "638632d23205375dcc757ba9",
        permissionsId: "638632e43205375dcc757bab",
        permissionsId: "638632e73205375dcc757bad",
        permissionsId: "638632eb3205375dcc757baf",
        permissionsId: "638632ef3205375dcc757bb1",
        permissionsId: "6386df5c0580e812c35c2490",
        permissionsId: "6386df600580e812c35c2492",
        permissionsId: "6386df670580e812c35c2494",
        permissionsId: "6386df6f0580e812c35c2496",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No debera remplazar un perfil especifico", (done) => {
    supertest(app)
      .patch("/profiles/63872dccd223515afa4b48fs")
      .send({
        description: "frontdeskk",
        status: true,
        permissionsId: "638632263205375dcc757b90",
        permissionsId: "638632393205375dcc757b93",
        permissionsId: "638632453205375dcc757b96",
        permissionsId: "638632513205375dcc757b99",
        permissionsId: "6386328d3205375dcc757b9b",
        permissionsId: "638632913205375dcc757b9d",
        permissionsId: "638632943205375dcc757b9f",
        permissionsId: "638632993205375dcc757ba1",
        permissionsId: "638632c83205375dcc757ba3",
        permissionsId: "638632cb3205375dcc757ba5",
        permissionsId: "638632cf3205375dcc757ba7",
        permissionsId: "638632d23205375dcc757ba9",
        permissionsId: "638632e43205375dcc757bab",
        permissionsId: "638632e73205375dcc757bad",
        permissionsId: "638632eb3205375dcc757baf",
        permissionsId: "638632ef3205375dcc757bb1",
        permissionsId: "6386df5c0580e812c35c2490",
        permissionsId: "6386df600580e812c35c2492",
        permissionsId: "6386df670580e812c35c2494",
        permissionsId: "6386df6f0580e812c35c2496",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar actualizar un perfil especifico", () => {
  it("Debera actualizar un perfil especifico", (done) => {
    supertest(app)
      .put("/profiles/63872dccd223515afa4b488e")
      .send({
        description: "frontdeskk",
        status: true,
        permissionsId: "638632263205375dcc757b90",
        permissionsId: "638632393205375dcc757b93",
        permissionsId: "638632453205375dcc757b96",
        permissionsId: "638632513205375dcc757b99",
        permissionsId: "6386328d3205375dcc757b9b",
        permissionsId: "638632913205375dcc757b9d",
        permissionsId: "638632943205375dcc757b9f",
        permissionsId: "638632993205375dcc757ba1",
        permissionsId: "638632c83205375dcc757ba3",
        permissionsId: "638632cb3205375dcc757ba5",
        permissionsId: "638632cf3205375dcc757ba7",
        permissionsId: "638632d23205375dcc757ba9",
        permissionsId: "638632e43205375dcc757bab",
        permissionsId: "638632e73205375dcc757bad",
        permissionsId: "638632eb3205375dcc757baf",
        permissionsId: "638632ef3205375dcc757bb1",
        permissionsId: "6386df5c0580e812c35c2490",
        permissionsId: "6386df600580e812c35c2492",
        permissionsId: "6386df670580e812c35c2494",
        permissionsId: "6386df6f0580e812c35c2496",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No debera actualizar un perfil especifico", (done) => {
    supertest(app)
      .put("/profiles/63872dccd223515afa4b48fs")
      .send({
        description: "frontdeskk",
        status: true,
        permissionsId: "638632263205375dcc757b90",
        permissionsId: "638632393205375dcc757b93",
        permissionsId: "638632453205375dcc757b96",
        permissionsId: "638632513205375dcc757b99",
        permissionsId: "6386328d3205375dcc757b9b",
        permissionsId: "638632913205375dcc757b9d",
        permissionsId: "638632943205375dcc757b9f",
        permissionsId: "638632993205375dcc757ba1",
        permissionsId: "638632c83205375dcc757ba3",
        permissionsId: "638632cb3205375dcc757ba5",
        permissionsId: "638632cf3205375dcc757ba7",
        permissionsId: "638632d23205375dcc757ba9",
        permissionsId: "638632e43205375dcc757bab",
        permissionsId: "638632e73205375dcc757bad",
        permissionsId: "638632eb3205375dcc757baf",
        permissionsId: "638632ef3205375dcc757bb1",
        permissionsId: "6386df5c0580e812c35c2490",
        permissionsId: "6386df600580e812c35c2492",
        permissionsId: "6386df670580e812c35c2494",
        permissionsId: "6386df6f0580e812c35c2496",
      })
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});

describe("Probar eliminar un perfil especifico", () => {
  it("Debera eliminar un perfil especifico", (done) => {
    supertest(app)
      .delete("/profiles/63872dccd223515afa4b488e")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("No debera eliminar un perfil especifico", (done) => {
    supertest(app)
      .delete("/profiles/63872dccd223515afa4b48fs")
      .set("Authorization", `Bearer ${TOKEN}`)
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
