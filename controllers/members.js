const express = require("express");
const Member = require("../models/member");
const Backlog = require("../models/backlog");
const bcrypt = require("bcrypt");
const Abilities = require("../models/ability");
const Profile = require("../models/profile");
const { ForbiddenError } = require("@casl/ability");

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Member);
      Member.find()
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.members"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.members"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Member);
      const id = req.params.id;
      Member.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.members")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.members")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function create(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Member);
      const email = req.body.email;
      const password = req.body.password;
      const fullName = req.body.fullName;
      const birthdate = req.body.birthdate;
      const curp = req.body.curp;
      const rfc = req.body.rfc;
      const address = new Object();
      const abilitiesId = req.body.abilitiesId;
      const profilesId = req.body.profilesId;

      let abilities = await Abilities.find({ "_id": abilitiesId });
      let profiles = await Profile.find({"_id": profilesId});

      address.street = req.body.street;
      address.intNumber = req.body.intNumber;
      address.extNumber = req.body.extNumber;
      address.zipCode = req.body.zipCode;
      address.state = req.body.state;
      address.country = req.body.country;

      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(password, salt);      

      let member = new Member({
        email: email,
        password: passwordHash,
        salt: salt,
        fullName: fullName,
        birthdate: new Date(birthdate),
        curp: curp,
        rfc: rfc,
        address: address,
        abilities: abilities,
        profiles: profiles,
      });

      member
        .save()
        .then((obj) =>
          res.status(200).json({
            message: res.__("create.members"),
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badCreate.members"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function replace(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Member);
      const id = req.params.id;

      const email = req.body.email ? req.body.email : "";
      const password = req.body.password ? req.body.password : "";
      // const salt = req.body.salt ? req.body.salt : "";

      const fullName = req.body.fullName ? req.body.fullName : "";
      const birthdate = req.body.birthdate ? req.body.birthdate : Date.now();
      const curp = req.body.curp ? req.body.curp : "";
      const rfc = req.body.rfc ? req.body.rfc : "";
      const address = new Object();
      const abilitiesId = req.body.abilitiesId ? req.body.abilitiesId : [];
      const profilesId = req.body.profilesId ? req.body.profilesId : [];

      let abilities = await Abilities.find({ _id: abilitiesId });
      let profiles = await Profile.find({ _id: profilesId });

      address.street = req.body.street ? req.body.street : "";
      address.intNumber = req.body.intNumber ? req.body.intNumber : "";
      address.extNumber = req.body.extNumber ? req.body.extNumber : "";
      address.zipCode = req.body.zipCode ? req.body.zipCode : -1;
      address.state = req.body.state ? req.body.state : "";
      address.country = req.body.country ? req.body.country : "";

      const salt = await bcrypt.genSalt(10);
      const passwordHashGen = await bcrypt.hash(password, salt);

      let member = new Object({
        _email: email,
        _password: passwordHashGen,
        _salt: salt,

        _fullName: fullName,
        _birthdate: birthdate,
        _curp: curp,
        _rfc: rfc,
        _address: address,
        _abilities: abilities,
        _profiles: profiles,
      });

      Member.findOneAndUpdate({ _id: id }, member, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("replace.members")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badReplace.members")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function update(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Member);
      const id = req.params.id;
      const email = req.body.email ? req.body.email : "";
      const password = req.body.password ? req.body.password : "";
      const salt = req.body.salt ? req.body.salt : "";
      const fullName = req.body.fullName;
      const birthdate = req.body.birthdate;
      const curp = req.body.curp;
      const rfc = req.body.rfc;
      const address = new Object();
      const abilitiesId = req.body.abilitiesId;
      const profilesId = req.body.profilesId;

      let abilities = await Abilities.find({ _id: abilitiesId });
      let profiles = await Profile.find({ _id: profilesId });

      address.street = req.body.street;
      address.intNumber = req.body.intNumber;
      address.extNumber = req.body.extNumber;
      address.zipCode = req.body.zipCode;
      address.state = req.body.state;
      address.country = req.body.country;

      let member = new Object();

      if (email) member._email = email;
      if (password) {
        const salt = await bcrypt.genSalt(10);
        const passwordHashGen = await bcrypt.hash(password, salt);
        member._password = passwordHashGen;
        member._salt = salt;
      }

      if (fullName) member._fullName = fullName;
      if (birthdate) member._birthdate = birthdate;
      if (curp) member._curp = curp;
      if (rfc) member._rfc = rfc;
      if (address) member._address = address;
      if (abilities) member._abilities = abilities;
      if (profiles) member._profiles = profiles;

      Member.findOneAndUpdate({ _id: id }, member, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("update.members")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badUpdate.members")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Member);
      const id = req.params.id;
      Member.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.members")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.members")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = { list, index, create, replace, update, destroy };
