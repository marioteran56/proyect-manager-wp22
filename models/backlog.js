const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _type: String,
    _stories: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Story'
    }]
});

class Backlog {
    constructor(type, stories){
        this._type = type;
        this._stories = stories;
    }

    get type(){
        return this._type;
    }

    set type(type){
        this._type = type;
    }

    get stories(){
        return this._stories;
    }

    set stories(stories){
        this._stories = stories;
    }

}

schema.loadClass(Backlog);
module.exports = mongoose.model('Backlog', schema);