const supertest = require("supertest");

const app = require("../app");

// SENTENCIA GENRAL

describe("Probar el sistema de Autentificacion", () => {
  it("Deberia obtener un login usuario y contraseña correctos", (done) => {
    supertest(app)
      .post("/login")
      .send({ email: "raul1234@gmail.com", password: "1234" })
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });

  it("NO deberia obtener un login usuario y contraseña correctos", (done) => {
    supertest(app)
      .post("/login")
      .send({ email: "raul1234@gmail.com", password: "12345" })
      .expect(403)
      .end(function (err, res) {
        if (err) done(err);
        else {
          done();
        }
      });
  });
});
