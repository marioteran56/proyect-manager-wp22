const express = require("express");
const Story = require("../models/story");
const Backlog = require("../models/backlog");
const { ForbiddenError } = require("@casl/ability");

const BacklogType = ["Product Backlog", "Release Backlog", "Sprint Backlog"];

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Backlog);
      Backlog.find()
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.backlogs"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.backlogs"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Backlog);
      const id = req.params.id;
      Backlog.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.backlogs")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.backlogs")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function create(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Backlog);
      const type = req.body.type;
      const storiesId = req.body.storiesId;

      let stories = await Story.find({ _id: storiesId });

      let backlog = new Backlog({
        type: type,
        stories: stories,
      });

      if (!BacklogType.includes(backlog.type)) {
        res.status(400).json({
          message: res.__("badBacklogType.backlogs"),
          obj: null,
        });
      } else {
        backlog
          .save()
          .then((obj) =>
            res.status(200).json({
              message: res.__("create.backlogs"),
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: res.__("badCreate.backlogs"),
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function replace(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Backlog);
      const id = req.params.id;
      const type = req.body.type ? req.body.type : "";
      const storiesId = req.body.storiesId ? req.body.storiesId : "";

      let stories = await Story.find({ _id: storiesId });

      let backlog = new Object({
        _type: type,
        _stories: stories,
      });

      if (!BacklogType.includes(backlog._type)) {
        res.status(400).json({
          message: res.__("badBacklogType.backlogs"),
          obj: null,
        });
      } else {
        Backlog.findOneAndUpdate({ _id: id }, backlog, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("replace.backlogs")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badReplace.backlogs")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function update(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Backlog);
      const id = req.params.id;
      const type = req.body.type;
      const storiesId = req.body.storiesId;

      let stories = await Story.find({ _id: storiesId });

      let backlog = new Object();

      if (type) backlog._type = type;
      if (stories) backlog._stories = stories;

      if (type && !BacklogType.includes(backlog._type)) {
        res.status(400).json({
          message: res.__("badBacklogType.backlogs"),
          obj: null,
        });
      } else {
        Backlog.findOneAndUpdate({ _id: id }, backlog, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("update.backlogs")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badUpdate.backlogs")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Backlog);
      const id = req.params.id;
      Backlog.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.backlogs")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.backlogs")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = { list, index, create, replace, update, destroy };
