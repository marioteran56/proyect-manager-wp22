const mongoose = require("mongoose");

const schema = mongoose.Schema({
  _email: String,
  _password: String,
  _salt: String,
  _fullName: String,
  _birthdate: Date,
  _curp: String,
  _rfc: String,
  _address: {
    street: String,
    intNumber: String,
    extNumber: String,
    zipCode: Number,
    state: String,
    country: String,
  },
  _abilities: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "Ability",
    },
  ],
  _profiles: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Profile",
    },
  ],
});

class Member {
  constructor(email,password,salt,fullName, birthdate, curp, rfc, address, abilities, profiles) {
    this._email = email;
    this._password = password;
    this._salt = salt;
    this._fullName = fullName;
    this._birthdate = birthdate;
    this._curp = curp;
    this._rfc = rfc;
    this._address = address;
    this._abilities = abilities;
    this._profiles = profiles;
  }

  get email() {
    return this._email;
  }

  set email(v) {
    this._email = v;
  }

  get password() {
    return this._password;
  }

  set password(v) {
    this._password = v;
  }

  get salt() {
    return this._salt;
  }

  set salt(v) {
    this._salt = v;
  }

  get fullName() {
    return this._fullName;
  }

  set fullName(fullName) {
    this._fullName = fullName;
  }

  get birthdate() {
    return this._birthdate;
  }

  set birthdate(birthdate) {
    this._birthdate = birthdate;
  }

  get curp() {
    return this._curp;
  }

  set curp(curp) {
    this._curp = curp;
  }

  get rfc() {
    return this._rfc;
  }

  set rfc(rfc) {
    this._rfc = rfc;
  }

  get address() {
    return this._address;
  }

  set address(address) {
    this._address = address;
  }

  get abilities() {
    return this._abilities;
  }

  set abilities(abilities) {
    this._abilities = abilities;
  }

  get profiles() {
    return this._profiles;
  }

  set profiles(v) {
    this._profiles = v;
  }
  
}

schema.loadClass(Member);
module.exports = mongoose.model("Member", schema);
