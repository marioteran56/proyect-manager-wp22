FROM node
MAINTAINER Mario Alberto Teran Acosta, Raul Hiram Pineda Chavez
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start