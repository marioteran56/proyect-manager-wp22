const express = require("express");
const Ability = require("../models/ability");
const { ForbiddenError } = require("@casl/ability");

const Rank = ["JUNIOR", "SENIOR", "MASTER"];

function list(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Ability);
      Ability.find()
        .then((objs) =>
          res.status(200).json({
            message: res.__("list.abilities"),
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: res.__("badList.abilities"),
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function index(req, res, next) {
  const id = req.params.id;
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", Ability);
      Ability.findOne({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("find.abilities")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badFind.abilities")}${id}`,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function create(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", Ability);
      const description = req.body.description;
      const rank = req.body.rank;

      if (!Rank.includes(rank)) {
        res.status(400).json({
          message: res.__("badAbilityType.abilities"),
          obj: null,
        });
      } else {
        let abilityObj = new Ability({
          description: description,
          rank: rank,
        });

        abilityObj
          .save()
          .then((obj) =>
            res.status(200).json({
              message: res.__("create.abilities"),
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: res.__("badCreate.abilities"),
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function replace(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Ability);
      const id = req.params.id;
      const description = req.body.description ? req.body.description : "";
      const rank = req.body.rank ? req.body.rank : "";

      if (!Rank.includes(rank)) {
        res.status(400).json({
          message: res.__("badAbilityType.abilities"),
          obj: null,
        });
      } else {
        let abilityObj = new Ability({
          description: description,
          rank: rank,
        });

        Ability.findOneAndUpdate({ _id: id }, abilityObj, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("replace.abilities")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badReplace.abilities")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

async function update(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", Ability);
      const id = req.params.id;
      const description = req.body.description;
      const rank = req.body.rank;

      let abilityObj = new Object();

      if (description) abilityObj._description = description;
      if (rank) abilityObj._rank = rank;

      if (rank != undefined && !Rank.includes(abilityObj._rank)) {
        res.status(400).json({
          message: res.__("badAbilityType.abilities"),
          obj: null,
        });
      } else {
        Ability.findOneAndUpdate({ _id: id }, abilityObj, { new: true })
          .then((obj) =>
            res.status(200).json({
              message: `${res.__("update.abilities")}${id}`,
              obj: obj,
            })
          )
          .catch((ex) =>
            res.status(403).json({
              message: `${res.__("badUpdate.abilities")}${id}`,
              obj: ex,
            })
          );
      }
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", Ability);
      const id = req.params.id;
      Ability.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `${res.__("destroy.abilities")}${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(403).json({
            message: `${res.__("badDestroy.abilities")}${id}`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(403).json({
          message: res.__("error.permission"),
          obj: error.message,
        });
      }
    }
  });
}

module.exports = { list, index, create, replace, update, destroy };
